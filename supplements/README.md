# Supplements web application

This is a simple webapp for retrieving supplement data either from a MongoDB database or from a JSON file stored on GitHub in case the database is not available.

A ready-made WAR file is already committed to the Docker project, so ideally there should not be any chances necessary in the source code. In case changes are a must:

- Import the project as Maven proejct to an IDE of your choice
- Make the necessary changes in either the ```Get.java``` or ```Init.java```
- Built the project via ```mvn package```
- Copy the ```supplements.war``` file to the /docker folder to replace the original WAR file

See the complete [XRebel APM demo script](https://docs.google.com/a/zeroturnaround.com/document/d/1mdsDi3mu3Zy9r_0TZLKD6llTXsePceIUlGWdfdrJRt0/edit?usp=sharing) for more information.

