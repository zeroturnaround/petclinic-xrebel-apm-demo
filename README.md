#XRebel APM Demo project

This repository contains 3 projects:

- Modified Sprint PetClinic (Maven project, running on Spring Boot)
- Supplements webapp (Maven project)
- Docker script for starting the Supplements webapp and MongoDB containers


See the complete [XRebel APM demo script](https://rwsoftware.sharepoint.com/:w:/r/product/_layouts/15/Doc.aspx?sourcedoc=%7B20702141-C5A3-4330-BAF9-C6D4479C3701%7D&file=Updated%20XRebel%20APM%20demo.docx&action=default&mobileredirect=true) for more information.